// Package truerand 尽可能取真随机数
package truerand

import (
	"crypto/rand"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"sync/atomic"
	"time"

	atomicbool "github.com/Andrew-M-C/go.atomicbool"
)

const (
	// 65535 is 0xFFFF
	strRemoteURLUint32    = "https://www.random.org/integers/?num=4&min=0&max=65535&col=1&base=16&format=plain&rnd=new"
	strRemoteURLUint8Tmpl = "https://www.random.org/integers/?num=%d&min=0&max=65535&col=1&base=16&format=plain&rnd=new"
)

var (
	errExceedQuota  = errors.New("quota used for today")
	errInvalidParam = errors.New("quota used for today")
)

// Rand 是暴露给调用方的接口对象
type Rand interface {
	Uint32() uint32
	Uint64() uint64
	Uint32s(n int) []uint32
	SetLocalMode()
}

// r 是 truerand 的实现对象。线程安全
type r struct {
	localMode   atomicbool.B
	quotaDate   atomic.Value // stores time.Time
	exceedQuota atomicbool.B
}

func (r *r) SetLocalMode() {
	r.localMode.Store(true)
}

// New 返回一个 Rand 对象
func New() Rand {
	return &r{}
}

func (r *r) Uint32() uint32 {
	arr := r.uint8N(4)

	var ret uint32
	ret += uint32(arr[0]) << 0
	ret += uint32(arr[1]) << 8
	ret += uint32(arr[2]) << 16
	ret += uint32(arr[3]) << 24
	return ret
}

func (r *r) Uint32s(n int) []uint32 {
	arr := r.uint8N(4 * n)

	ret := make([]uint32, n)
	for i := 0; i < n; i++ {
		off := 4 * i
		var num uint32
		num += uint32(arr[off+0]) << 0
		num += uint32(arr[off+1]) << 8
		num += uint32(arr[off+2]) << 16
		num += uint32(arr[off+3]) << 24
		ret[i] = num
	}

	return ret
}

func (r *r) Uint64() uint64 {
	return 0
}

func (r *r) uint32FromRemote() (ret uint32, err error) {
	// read 4 uint8 integers
	arr, err := r.uint8RemoteN(4)
	if err != nil {
		return
	}

	offset := 0
	for _, u := range arr {
		ret += uint32(u) << offset
		offset += 8
	}

	return
}

func (r *r) uint8N(n int) (ret []uint8) {
	if r.localMode.Load() {
		ret, _ = r.uint8LocalN(n)
		return
	}
	if r.exceedQuota.Load() {
		// TODO: 判断是否跨天了
		ret, _ = r.uint8LocalN(n)
		return
	}

	ret, err := r.uint8RemoteN(n)
	if err != nil {
		if err == errExceedQuota {
			r.exceedQuota.Store(true)
			r.quotaDate.Store(time.Now())
		}
	}
	ret, _ = r.uint8LocalN(n)
	return
}

func (r *r) uint8RemoteN(n int) (ret []uint8, err error) {
	if n < 0 {
		err = errInvalidParam
		return
	}
	u := fmt.Sprintf(strRemoteURLUint8Tmpl, n)
	resp, err := http.Get(u)
	if err != nil {
		return
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}

	s := string(body)
	if strings.Contains(s, "quota") {
		err = errExceedQuota
		return
	}

	parts := strings.Split(s, "\n")
	if len(parts) < n {
		err = fmt.Errorf("unexpected return length %d", len(parts))
		return
	}

	ret = make([]uint8, 0, n)
	parts = parts[:n]
	for _, s := range parts {
		num, _ := strconv.ParseUint(s, 16, 32)
		ret = append(ret, uint8(num))
	}

	return ret, nil
}

func (r *r) uint8LocalN(n int) (ret []uint8, err error) {
	arr := make([]byte, n)
	_, err = rand.Read(arr)
	if err != nil {
		return
	}

	ret = make([]uint8, n)
	for i, b := range arr {
		ret[i] = uint8(b)
	}
	return
}
