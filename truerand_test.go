package truerand

import (
	"testing"
)

func TestRand(t *testing.T) {
	r := New()

	testRand(r, t)

	r.SetLocalMode()
	testRand(r, t)

	return
}

func testRand(r Rand, t *testing.T) {
	for i := 0; i < 5; i++ {
		n := r.Uint32()
		t.Logf("[%02d] - %d, %x", i, n, n)
	}

	arr := r.Uint32s(10)
	for i, n := range arr {
		t.Logf("[%02d] - %d, %x", i, n, n)
	}

	return
}
